# Analyzes hashtag data base on two parameters: Mastodon instance and hashtag
# Usage: python3 hashtag_info.py mastodon.online hashtag

import requests
import json
import csv
import sys
from collections import Counter
from tabulate import tabulate 
from datetime import date
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
import itertools
import networkx as nx
import numpy as np
from urlextract import URLExtract


mentions_string = " "
autor_string = " "
repliedto_string = " "
createdby_string = " "
dupla = " "

repliedto = []
conversa = []
quemfalou = []
tags = []
replyto = []
mentions = [] 
createdby = [] 
replies = []
mentions_simple = []

count_createdby = Counter()
count_tags = Counter()
count_mentions = Counter()
count_replies = Counter()

pairslist = []

extractor = URLExtract()
urls = [ ]

today = date.today()
instance = str(sys.argv[1]) # mastodon instance
param = str(sys.argv[2]) # hashtag escolhida
iter = 1 # para nome de arquivo/output

base_url = "https://" +instance+ "/api/v1/timelines/tag/" + param # define URL com base nos parâmetros
print("Path -> "+base_url) # notifica usuário, URL

response = requests.get(base_url) # pega conteúdo e headers (20 é limite Mastodon)
#response = requests.get(base_url, headers={"content-type":"text"}) # pega conteúdo e headers (20 é limite Mastodon)
#response = requests.get("https://mastodon.online/api/v1/timelines/tag/edabertamp")
print(response.request.headers)
statuses = json.loads(response.text) # converte json para lista de dicionários python



while "next" in response.links: # enquanto houver nova página com mais statuses

 for x in range(len(statuses)):
   # urls = extractor.gen_urls(statuses[x]["content"]): 
   createdby.append(statuses[x]["account"]["username"])
   createdby_string = ''.join(statuses[x]["account"]["username"]) # pega autor, para criar pares de autoria-resposta
  
   if statuses[x]["in_reply_to_account_id"] != None:
     replies.append(statuses[x]["account"]["username"]) # quem postou a mensagem
     repliedto.append(statuses[x]["in_reply_to_account_id"])
     repliedto_string = ''.join("<a href=\"https://mastodon.online/web/accounts/"+statuses[x]["in_reply_to_account_id"]+"\">"+statuses[x]["in_reply_to_account_id"]+"</a>") # pega à quem foi a resposta, para criar pares de autoria-resposta
     dupla = (createdby_string, repliedto_string) # guarda cada um do pares de autoria-resposta
     conversa.insert(0, dupla) # guarda os pares de conversas (autoria-resposta)
   
   for y in range(len(statuses[x]["tags"])):
     tags.append(statuses[x]["tags"][y]["name"])     
   
   if statuses[x]["mentions"]:
    for y in range(len(statuses[x]["mentions"])): 
      mentions.append(statuses[x]["mentions"][y]["username"]) # quem foi mencionado
      mentions_simple.append(statuses[x]["mentions"][y]["username"]) # temporário para pares, vai ser apagado
      pairs = (statuses[x]["account"]["username"], statuses[x]["mentions"][y]["username"]) # pares para criar gráficos
      pairslist.insert(0, pairs) # junta todos os pares
    mentions_string = mentions_string.join(mentions_simple) # limpa menções para inserção na lista
    tuple = (statuses[x]["account"]["username"], mentions_string) # criar tuple para de autor e à quem foi respondido
    quemfalou.insert(0, tuple) # cria lista completa de autores e à quem foi respondido

   
   mentions_simple = [] 
   mentions_string = " "
   createdby_string = " "
   repliedto_string = " "
   
 base_url = (response.links["next"]["url"]) # pega a próxima página
 #response = requests.get(base_url, headers={"content-type":"text"}) # alguma coisa mudou e ele encrenca com headers
 response = requests.get(base_url)
 statuses = json.loads(response.text) 

quemfalou.sort()
conversa.sort()

G = nx.DiGraph()
#for node_tuple in pairslist:
G.add_edges_from(pairslist)
#itertools.product(node_tuple, node_tuple))
f = plt.figure()
nx.draw_circular(G, with_labels=True, node_size=300, alpha=0.3, arrows=True, edge_color = 'b')  
#plt.show()
f.savefig("graph.png")

# Note: Next step is to optimize this together with HTML generation
for item in createdby:
 count_createdby[item] +=1
createdby_count = count_createdby.most_common()

for item in replies:
 count_replies[item] +=1
replies_count = count_replies.most_common()

for item in tags: # conta e ordena a lista de tags mencionadas
 count_tags[item] +=1
tags_count = count_tags.most_common(10) 

for item in mentions: # conta e ordena a lista de tags mencionadas
 count_mentions[item] +=1
mentions_count = count_mentions.most_common() 

temp_counter = Counter()

for item in conversa:
 temp_counter[item] +=1
conversa_count = temp_counter.most_common()

graphnames = [createdby_count, replies_count, tags_count, mentions_count] # se mudar os items de coleta, mude aqui também
for x in graphnames: 
 plt.style.use('ggplot')
 grf = plt.subplot() # subplot permite mais flexibilidade com gráficos
 grf.bar(*zip(*x), color='grey') # cria gráfico em barra
 plt.xticks(rotation=30, ha='right') # rotaciona os descritivos em 'x'
 plt.yticks() #define mínimo, máximo e intervalo 
 # plt.show()
 plt.xlabel('User')
 plt.ylabel('Times')
 filename = str(iter)+".jpg"
 plt.tight_layout(pad=5) # ajusta tamanho da imagem

 plt.savefig(filename)
 plt.clf() # limpa o buffer dos gráficos para não criar duplicação
 iter += 1 # muda nome do arquivo

with open('index.html', 'w') as f:
 f.write("<html><body>")
 f.write("<head><meta charset=\"UTF-8\"><link rel=\"stylesheet\" href=\"table.css\"><meta charset=\"UTF-8\"></head>")
 f.write("<h4>Hashtag: <i>"+param+"</i><br>Server: "+instance+"<br>Day: "+today.strftime("%d/%m/%Y"))
 f.write("<h4>Most publications</h4><br>")
 f.write("<img src=\"1.jpg\">")
 f.write(tabulate(createdby_count, headers=["User","Times"], tablefmt="html"))
 f.write("<h4>Most responded to others</h4>")
 f.write("<img src=\"2.jpg\">")
 f.write(tabulate(replies_count, headers=["Users","Times"], tablefmt="html"))
 f.write("<br><br>")
 #f.write(tabulate(quemfalou, headers=["Author", "Answered tablefmt="html""], to))
 f.write("<h4>Relações: Who mentions who</h4>")
 f.write("<img src=\"graph.png\">")
 f.write("<br><br>")
 f.write("<h4>Relations: Direct responses to others</h4>")
 f.write("<br><br>")
 f.write(tabulate(conversa_count, headers=["Author, Responded to", "Times"], tablefmt="unsafehtml"))
 f.write("<h4>Top 10 used tags</h4>")
 f.write("<img src=\"3.jpg\">")
 f.write(tabulate(tags_count, headers=["Users","Times"], tablefmt="html"))
 f.write("<h4>Most mentioned</h4>")
 f.write("<img src=\"4.jpg\">")
 f.write(tabulate(mentions_count, headers=["Users","Times"], tablefmt="html"))
 f.write("</body></html>")
