# Mastodon counter

Gets Mastodon statuses data based on a defined **hashtag** and provides a clean HTML file with tables and graphs (who authored, who most replied to others, other hashtags, who was replied to).

Install pip:
`sudo apt install python3-pip`

Install dependencies:
`pip install tabulate`
`pip install matplotlib`
`pip install networkx`
`pip install urlextract`

* Usage: python3 hashtag_info.py mastodon_instance hashtag
* Example: python3 hashtag_info.py mastodon.online abertaunb

Live view example: https://amiel.net.br/scripts/oes2021/
